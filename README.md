### Description du projet 

Ce projet consiste à créer un cluster de machines identiques attachées à un load blancer sur le cloud Azure. L 'outil utilsé pour créer l 'infrastructure est terraform, après on vise à configurer le cluster des machines virtuel avec ansible. L'objectif final est d 'orchestrer terraform et ansible avec jenkins.
On va aussi  utiliser une VM sur le même réseau qui va servir à la configuration de notre cluster.
##### 1 ére étape: la creation de l'infrastructure sur azure avec terraform.
Après avoir créé le compte azure et configurer azure Access pour utiliser terraform:

```
provider "azurerm" {
subscription_id = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
client_id = "3XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
client_secret = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
tenant_id = "4XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX2"
}


```
###### Variables:

On commence par la définition du ficher de variables. Dans cette section on va définir les vraibles qui vont être utilisé pour configurer les ressources créées par terraform:


```variable "location" {
 description = "The location where resources will be created"
 default= "West Europe"
}

variable "tags" {
 description = "A map of the tags to use for the resources that are deployed"
 type        = "map"

 default = {
   environment = "codelab"
 }
}

variable "resource_group_name" {
 description = "The name of the resource group in which the resources will be created"
 default     = "myResourceGroup"
}

variable "application_port" {
   description = "The port that you want to expose to the external load balancer"
   default     = 80
}

variable "admin_user" {
   description = "User name to use as the admin account on the VMs that will be part of the VM Scale Set"
   default     = "azureuser"
}

variable "admin_password" {
   description = "Default password for admin account"
   default     = "azureuser123!"
   
```

##### Output 

On aura un fichier `output.tf`  qui va décrire la sortie après le déploiement de notre infrastructure:
les sorties sont le FQDN du cluster de machines virtuelles et FQDN de la machine de configuration Jumpbox

```
output "vmss_public_ip" {
     value = "${azurerm_public_ip.vmss.fqdn}"
 }
output "jumpbox_public_ip" {
   value = "${azurerm_public_ip.jumpbox.fqdn}"
}
```

#### Infrastructure Network 

Dans cette section on a créé dans un nouveau Azure resource group:

*  Un seul réseau virtuel avec un espace d 'adressage de 10.0.0.0/16
*  Un seul sous réseau avec l 'espace d'adressage 10.0.2.0/24
* Deux adresses IP. Une est utilisé pour le scale set load balancer, et l 'autre est utilisé pour la machine de configuration Jumpbox

```resource "azurerm_resource_group" "vmss" {
 name     = "${var.resource_group_name}"
 location = "${var.location}"
 tags     = "${var.tags}"
}

resource "azurerm_virtual_network" "vmss" {
 name                = "vmss-vnet"
 address_space       = ["10.0.0.0/16"]
 location            = "${var.location}"
 resource_group_name = "${azurerm_resource_group.vmss.name}"
 tags                = "${var.tags}"
}

resource "azurerm_subnet" "vmss" {
 name                 = "vmss-subnet"
 resource_group_name  = "${azurerm_resource_group.vmss.name}"
 virtual_network_name = "${azurerm_virtual_network.vmss.name}"
 address_prefix       = "10.0.2.0/24"
}

resource "azurerm_public_ip" "vmss" {
 name                         = "vmss-public-ip"
 location                     = "${var.location}"
 resource_group_name          = "${azurerm_resource_group.vmss.name}"
 allocation_method = "Static"
 domain_name_label            = "${random_string.fqdn.result}"
 tags                         = "${var.tags}"
}


```
##### Cluster des machines virtuelles identiques (vm scale set )
 Dans cette section on va créer les ressources suivantes:
* Azure load Balancer attaché à une adresse IP public 
* Azure backend address assigner au load balancer 
* A health probe configurer dans le load balancer 
* 3 Machine Virtuelles (scale set) qui tourneront derrière le load balancer

```resource "azurerm_lb" "vmss" {
 name                = "vmss-lb"
 location            = "${var.location}"
 resource_group_name = "${azurerm_resource_group.vmss.name}"

 frontend_ip_configuration {
   name                 = "PublicIPAddress"
   public_ip_address_id = "${azurerm_public_ip.vmss.id}"
 }

 tags = "${var.tags}"
}

resource "azurerm_lb_backend_address_pool" "bpepool" {
 resource_group_name = "${azurerm_resource_group.vmss.name}"
 loadbalancer_id     = "${azurerm_lb.vmss.id}"
 name                = "BackEndAddressPool"
}

resource "azurerm_lb_probe" "vmss" {
 resource_group_name = "${azurerm_resource_group.vmss.name}"
 loadbalancer_id     = "${azurerm_lb.vmss.id}"
 name                = "ssh-running-probe"
 port                = "${var.application_port}"
}

resource "azurerm_lb_rule" "lbnatrule" {
   resource_group_name            = "${azurerm_resource_group.vmss.name}"
   loadbalancer_id                = "${azurerm_lb.vmss.id}"
   name                           = "http"
   protocol                       = "Tcp"
   frontend_port                  = "${var.application_port}"
   backend_port                   = "${var.application_port}"
   backend_address_pool_id        = "${azurerm_lb_backend_address_pool.bpepool.id}"
   frontend_ip_configuration_name = "PublicIPAddress"
   probe_id                       = "${azurerm_lb_probe.vmss.id}"
}

resource "azurerm_virtual_machine_scale_set" "vmss" {
 name                = "vmscaleset"
 location            = "${var.location}"
 resource_group_name = "${azurerm_resource_group.vmss.name}"
 upgrade_policy_mode = "Manual"

 sku {
   name     = "Standard_DS1_v2"
   tier     = "Standard"
   capacity = 3
 }

 storage_profile_image_reference {
   publisher = "Canonical"
   offer     = "UbuntuServer"
   sku       = "16.04-LTS"
   version   = "latest"
 }

 storage_profile_os_disk {
   name              = ""
   caching           = "ReadWrite"
   create_option     = "FromImage"
   managed_disk_type = "Standard_LRS"
 }

 storage_profile_data_disk {
   lun          = 0
   caching        = "ReadWrite"
   create_option  = "Empty"
   disk_size_gb   = 10
 }

 os_profile {
   computer_name_prefix = "vmlab"
   admin_username       = "${var.admin_user}"
   admin_password       = "${var.admin_password}"
 }

 os_profile_linux_config {
   disable_password_authentication = false
 }

 network_profile {
   name    = "terraformnetworkprofile"
   primary = true

   ip_configuration {
     name                                   = "IPConfiguration"
     subnet_id                              = "${azurerm_subnet.vmss.id}"
     load_balancer_backend_address_pool_ids = ["${azurerm_lb_backend_address_pool.bpepool.id}"]
     primary = true
   }
 }

 tags = "${var.tags}"
}


resource "azurerm_public_ip" "jumpbox" {
 name                         = "jumpbox-public-ip"
 location                     = "${var.location}"
 resource_group_name          = "${azurerm_resource_group.vmss.name}"
 allocation_method = "Static"
 domain_name_label            = "${random_string.fqdn.result}-ssh"
 tags                         = "${var.tags}"
}

```

##### La machine de configuration Jumpbox
La machine Jumpbox c 'est le serveur  qui va nous permettre d 'accéder au cluster des machines identiques pour les configurer 
cette machine sera configurer avec les ressources suivantes:
* une interface réseau connecté au  même sous réseau que les machines identiques scale set
* Jumpbox est accessible à distance via la connection SSH:

```resource "azurerm_network_interface" "jumpbox" {
 name                = "jumpbox-nic"
 location            = "${var.location}"
 resource_group_name = "${azurerm_resource_group.vmss.name}"

 ip_configuration {
   name                          = "IPConfiguration"
   subnet_id                     = "${azurerm_subnet.vmss.id}"
   private_ip_address_allocation = "dynamic"
   public_ip_address_id          = "${azurerm_public_ip.jumpbox.id}"
 }

 tags = "${var.tags}"
}

resource "azurerm_virtual_machine" "jumpbox" {
 name                  = "jumpbox"
 location              = "${var.location}"
 resource_group_name   = "${azurerm_resource_group.vmss.name}"
 network_interface_ids = ["${azurerm_network_interface.jumpbox.id}"]
 vm_size               = "Standard_DS1_v2"

 storage_image_reference {
   publisher = "Canonical"
   offer     = "UbuntuServer"
   sku       = "16.04-LTS"
   version   = "latest"
 }

 storage_os_disk {
   name              = "jumpbox-osdisk"
   caching           = "ReadWrite"
   create_option     = "FromImage"
   managed_disk_type = "Standard_LRS"
 }

 os_profile {
   computer_name  = "jumpbox"
   admin_username = "${var.admin_user}"
   admin_password = "${var.admin_password}"
 }

 os_profile_linux_config {
   disable_password_authentication = false
 }

 tags = "${var.tags}"
}
```
##### Provisionner notre infrastructure 
initialisé le repertoire de travail terraform 

```terraform init```

générer et afficher le plan d 'execution de l 'infrastructure 

```terraform plan```

Deployer mon infrastructure 

```terraform apply```

#### le graphe de l 'infrastructure déployer:

![alt text](graphviz.png)

## 2 éme étape: Configuration avec ansible

On commence par installer ansible sur la machine de configuration Jumpbox

``` 
## Install pre-requisite packages
sudo yum check-update; sudo yum install -y gcc libffi-devel python-devel openssl-devel epel-release
sudo yum install -y python-pip python-wheel

## Install Ansible and Azure SDKs via pip
sudo pip install ansible[azure]
```

## configuration des credentiels 



```
export AZURE_SUBSCRIPTION_ID=<your-subscription_id>
export AZURE_CLIENT_ID=<security-principal-appid>
export AZURE_SECRET=<security-principal-password>
export AZURE_TENANT=<security-principal-tenant>
```





Crée un script ansible `get-hosts.yml` pour récupérer automatiquement les informations nécessaires pour accéder au cluster (inventory dynamique) de machine scale set.



Utiliser un script ansible `application.yml` pour préparer une simple application pour le déploiement.




Deployer l'application sur le cluster des machines identiques `deploy.yml`




